
# Use to add dotfiles to new setup
# curl -Lks https://gitlab.com/treppert/dotfiles/-/raw/master/.bin/dotfiles_setup.sh | /bin/bash

git clone --bare git@gitlab.com:treppert/dotfiles.git $HOME/.dotfiles
function config {
   /usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME $@
}
mkdir -p .dotfiles-backup
config checkout
if [ $? = 0 ]; then
  echo "Checked out config.";
  else
    echo "Please backup your pre-existing dot files safely first"
    # echo "Backing up pre-existing dot files.";
    # config checkout 2>&1 | egrep "\s+\." | awk {'print $1'} | xargs -I{} cp {} .dotfiles-backup/{}
    # config checkout
fi;
config config status.showUntrackedFiles no

