# .bashrc

# Source global definitions
if [ -f /etc/skel/.bashrc ]; then
	. /etc/skel/.bashrc
fi

# User specific environment
if ! [[ "$PATH" =~ "$HOME/.local/bin:$HOME/bin:" ]]
then
    PATH="$HOME/.local/bin:$HOME/bin:$PATH"
fi
export PATH

red='\[\e[0;31m\]'
RED='\[\e[1;31m\]'
blue='\[\e[0;34m\]'
BLUE='\[\e[1;34m\]'
cyan='\[\e[0;36m\]'
CYAN='\[\e[1;36m\]'
green='\[\e[0;32m\]'
GREEN='\[\e[1;32m\]'
yellow='\[\e[0;33m\]'
YELLOW='\[\e[1;33m\]'
PURPLE='\[\e[1;35m\]'
purple='\[\e[0;35m\]'
nc='\[\e[0m\]'

if [ "$UID" = 0 ]; then
    PS1="$red\u$nc@$red\H$nc:$CYAN\w$nc\\n$red#$nc "
else
    PS1="$green\u$nc@$BLUE\H$nc:$CYAN\w$nc\\n$GREEN\$$nc "
fi



# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=

# User specific aliases and functions
# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
#__conda_setup="$('/home/treppert/anaconda3/bin/conda' 'shell.bash' 'hook' 2> /dev/null)"
#if [ $? -eq 0 ]; then
#    eval "$__conda_setup"
#else
#    if [ -f "/home/treppert/anaconda3/etc/profile.d/conda.sh" ]; then
#        . "/home/treppert/anaconda3/etc/profile.d/conda.sh"
#    else
#        export PATH="/home/treppert/anaconda3/bin:$PATH"
#    fi
#fi
#unset __conda_setup
# <<< conda initialize <<<

alias config='/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'
export PIPENV_VENV_IN_PROJECT=1
export PATH=$HOME/bin:$HOME/.bin:$HOME/.cargo/bin:$PATH
/usr/bin/neofetch

